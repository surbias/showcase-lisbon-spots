//
//  UIImage+ImageAsync.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 18/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "UIImage+ImageAsync.h"

@implementation UIImage (ImageAsync)

+ (void)loadAsyncFromURL:(NSString *)aURLString
           withImageView:(UIImageView *)anImageView
                withName: (NSString *)aName
    andActivityIndicator:(UIActivityIndicatorView *)anIndicator{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *anImage = [self loadFromCacheWithName:aName];
        if (!anImage) {
            NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [NSString stringWithFormat:@"%@", aURLString]]];
            anImage = [UIImage imageWithData:imageData];
            
            [UIImage saveToCache:anImage withName:aName];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (anIndicator) {
                [anIndicator stopAnimating];
            }
            
            anImageView.image = anImage;
        });
    });

}

+ (void)saveToCache:(UIImage *)anImage
           withName:(NSString *)aName{
    NSString *path = [NSString stringWithFormat:@"%@%@", [self applicationDocumentDirectory], aName];
    
    BOOL found = [[NSFileManager defaultManager] createFileAtPath:path
                                                      contents:nil
                                                       attributes:nil];
    if (!found) {
        NSLog(@"Error create file @ %@", path);
    } else{
        NSFileHandle* fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
        [fileHandle writeData:UIImageJPEGRepresentation(anImage, 1.0)];
        [fileHandle closeFile];
    }
}

+ (UIImage *)loadFromCacheWithName:(NSString *)aName{
    NSString *path = [NSString stringWithFormat:@"%@%@", [self applicationDocumentDirectory], aName];

    NSFileHandle* myFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    
    return [UIImage imageWithData:[myFileHandle readDataToEndOfFile]];

}

+ (NSString *)applicationDocumentDirectory {
    //Returns the path to the application's documents directory.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

@end
