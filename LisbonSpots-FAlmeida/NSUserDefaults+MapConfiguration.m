//
//  NSUserDefaults+MapConfiguration.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "NSUserDefaults+MapConfiguration.h"
#import <MapKit/MapKit.h>
@implementation NSUserDefaults (MapConfiguration)

+ (NSUInteger)mapTypeWithIdentifier:(NSUInteger)identifier{
    switch (identifier) {
        case 0:
            return MKMapTypeStandard;
        case 1:
            return MKMapTypeHybrid;
        case 2:
            return MKMapTypeSatellite;
        default:
            return MKMapTypeStandard;
    }

}

+ (NSUInteger)mapAccuracyWithIdentifier:(NSUInteger)identifier{
    switch (identifier) {
        case 0:
            return kCLLocationAccuracyNearestTenMeters;
            break;
        case 1:
            return kCLLocationAccuracyHundredMeters;
            
        case 2:
            return kCLLocationAccuracyKilometer;
        default:
            return kCLLocationAccuracyHundredMeters;
    }
}

+ (NSUInteger)distanceFilterWithIdentifier:(NSUInteger)identifier{
    switch (identifier) {
        case 0:
            return kCLDistanceFilterNone;
        case 1:
            return CLLocationDistanceMax;
        default:
            return kCLDistanceFilterNone;
    }
}

@end
