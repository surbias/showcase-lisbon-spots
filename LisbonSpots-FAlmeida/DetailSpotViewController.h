//
//  DetailSpotViewController.h
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 15/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Spot.h"

@interface DetailSpotViewController : UIViewController

//navigation - needs to be *public* since
//it needs to be acessed from the segue
@property Spot *selectedSpot;

@end
