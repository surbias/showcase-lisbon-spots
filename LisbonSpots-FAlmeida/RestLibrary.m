//
//  RestLibrary.m
//  LisbonSpots-FAlmeida
//
//  Created by Formando FLAG on 14/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "RestLibrary.h"
#import "Spot.h"
#import "Constants.h"

@implementation RestLibrary

+ (NSURL *)spotsUrl{
    return [NSURL URLWithString:kMY_URL];
}

+ (BOOL)getSpots{
    if ([[Spot fetchAllSpots] count] == 0) {

        NSURLRequest *request = [NSURLRequest requestWithURL:[RestLibrary spotsUrl]];

        NSHTTPURLResponse *response;
        NSError *error;
        NSData *result = [NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:&error];

        if (error == nil && response.statusCode != 200) {
            NSLog(@"HTTP Response: %@", response);
            NSLog(@"Error: %@", [error localizedDescription]);
            return NO;
        }

        NSArray *spotsJSONArray = [NSJSONSerialization JSONObjectWithData:result
                                        options:0
                                          error:&error];

        if (error != nil) {
            NSLog(@"Parse Error: %@", [error localizedDescription]);
            return NO;
        }

        for (NSDictionary *dict in spotsJSONArray) {
            [Spot spotWithDictionary:dict];
        }
    }
    
    return YES;
}

@end
