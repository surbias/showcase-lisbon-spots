//
//  UIImage+ImageAsync.h
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 18/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Spot.h"
@interface UIImage (ImageAsync)

+ (void)loadAsyncFromURL:(NSString *)aURLString
           withImageView:(UIImageView *)anImageView
                withName: (NSString *)aName
    andActivityIndicator:(UIActivityIndicatorView *)anIndicator;

+ (void)saveToCache:(UIImage *)anImage
           withName:(NSString *)aName;

+ (UIImage *)loadFromCacheWithName:(NSString *)aName;

@end
