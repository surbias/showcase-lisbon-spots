//
//  DetailSpotViewController.m
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 15/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "DetailSpotViewController.h"
#import "UIImage+ImageAsync.h"
#import "Favorite.h"

@interface DetailSpotViewController()

//UI
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *detailInfoView;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavigationItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *favoriteBarButtonItem;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingIndicator;

- (IBAction)dismiss:(UIBarButtonItem *)sender;
- (IBAction)favoriteTapped:(UIBarButtonItem *)sender;

//Temporary
@property BOOL hasFavorite;

@end

@implementation DetailSpotViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    
    if (_selectedSpot) {
        
        _titleNavigationItem.title = _selectedSpot.name;
        _detailInfoView.backgroundColor = [Spot getColorSpot:_selectedSpot];

        [UIImage loadAsyncFromURL:_selectedSpot.imageURL
                    withImageView:_imageView
                         withName:[NSString stringWithFormat:@"%d",_selectedSpot.identifier]
             andActivityIndicator:_imageLoadingIndicator];

        _nameLabel.text = _selectedSpot.name;
        _phoneLabel.text = _selectedSpot.phone;
        _descriptionLabel.text = _selectedSpot.details;
        
        //This is here only for the first time
        _hasFavorite = [Favorite hasFavoriteWithidentifier:_selectedSpot.identifier];
        _favoriteBarButtonItem.image = [self setFavoriteImage:_hasFavorite];
        
    }

}
- (IBAction)dismiss:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)favoriteTapped:(UIBarButtonItem *)sender {
    _hasFavorite = [Favorite hasFavoriteWithidentifier:_selectedSpot.identifier];
    
    if (_hasFavorite) {
        [Favorite removeFavoriteWithIdentifer:_selectedSpot.identifier];
    }else{
        [Favorite favoriteWithSpotIdentifier:_selectedSpot.identifier];
    }
    
    _hasFavorite = !_hasFavorite;
    sender.image = [self setFavoriteImage:_hasFavorite];
    
}

- (UIImage *)setFavoriteImage:(BOOL)hasFavorite{
    
    if (hasFavorite) {
        return [UIImage imageNamed:@"IconFavoriteFill"];
    } else{
        return [UIImage imageNamed:@"IconFavoriteStroke"];
    }

}
@end
