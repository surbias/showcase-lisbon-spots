//
//  Spot.m
//  LisbonSpots-FAlmeida
//
//  Created by Formando FLAG on 14/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import "Spot.h"
#import "AppDelegate.h"
#import "Constants.h"

@implementation Spot

@dynamic identifier;
@dynamic type;
@dynamic name;
@dynamic phone;
@dynamic imageURL;
@dynamic details;
@dynamic latitude;
@dynamic longitude;

+ (instancetype)spotWithDictionary:(NSDictionary *)aDictionary{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    //Create a new instance at core data
    Spot *aSpot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:objectContext];
    
    //Initialize newSpot's properties
    if (aSpot) {
        
        aSpot.identifier = [aDictionary[@"id"] floatValue];
        aSpot.type = aDictionary[@"type"];
        aSpot.name = aDictionary[@"name"];
        aSpot.phone = aDictionary[@"phone"];
        aSpot.details = aDictionary[@"desc"];
        aSpot.imageURL = aDictionary[@"img_url"];
        aSpot.latitude = [aDictionary[@"latitude"] doubleValue];
        aSpot.longitude = [aDictionary[@"longitude"] doubleValue];
        
    }
    
    //commits initialization
    [appDelegate saveContext];
    
    return aSpot;
}

+ (NSArray *)fetchAllSpots{
    return [Spot fetchAllSpotsWithType:nil];
}

+ (void)removeAllSpots{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    for (Spot *spot in [Spot fetchAllSpots]) {
        [objectContext deleteObject:spot];
    }
    
    [appDelegate saveContext];
}

+ (NSArray *)fetchAllSpotsWithType:(NSString *)aType{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    
    if (aType != nil) {
        //executes pseudo sql query
        fetch.predicate = [NSPredicate predicateWithFormat:@"type == %@", aType];
    }
    
    NSArray *results = [objectContext executeFetchRequest:fetch error:&error];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return nil;
    }
    
    return results;
}

+ (Spot *)fetchSpotWithIdentifier:(int32_t)identifier{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *objectContext = appDelegate.managedObjectContext;
    
    NSError *error;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    
    fetch.fetchLimit = 1;
    fetch.predicate = [NSPredicate predicateWithFormat:@"identifier == %d", identifier];
    
    NSArray *results = [objectContext executeFetchRequest:fetch error:&error];
    
    if (error != nil) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return nil;
    }
    if ([results count] != 0) {
        return results[0];
    }else{
        return nil;
    }
}


+ (UIImage *) getTypeIconFromSpot:(Spot *)aSpot{
    UIImage *iconImage;
    
    if ([aSpot.type isEqualToString:@"bar"]) {
        iconImage = [UIImage imageNamed:@"IconBar"];
    } else if([aSpot.type isEqualToString:@"club"]){
        iconImage = [UIImage imageNamed:@"IconClub"];
    } else if([aSpot.type isEqualToString:@"restaurant"]){
        iconImage = [UIImage imageNamed:@"IconRestaurant"];
    }
    
    return iconImage;
}

+ (UIColor *) getColorSpot:(Spot *) aSpot{
    UIColor *aColor = [[UIColor alloc]init];

    
    if ([aSpot.type isEqualToString:@"bar"]) {
        aColor =kCOLOR_SKY_BLUE;
    } else if([aSpot.type isEqualToString:@"club"]){
        aColor = kCOLOR_LAVENDER;
    } else if([aSpot.type isEqualToString:@"restaurant"]){
        aColor = kCOLOR_CANTALOUPE;
    }
    else {
        aColor = [UIColor clearColor];
    }
    return aColor;

}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ [%@] (%f, %f)",
            self.name, self.type, self.latitude, self.longitude];
    
}

#pragma Mark - MKAnnotation methods
- (CLLocationCoordinate2D)coordinate{
    return  CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

- (NSString *)title{
    return [NSString stringWithFormat:@"%@", [self.name capitalizedString]];
}

- (NSString *)subtitle{
    return [NSString stringWithFormat:@"Contacts: %@", self.phone];
}


@end
