//
//  Favorite.h
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 19/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Favorite : NSManagedObject

@property (nonatomic) int32_t spotIdentifier;
@property (nonatomic) NSTimeInterval dateAdded;

+ (instancetype)favoriteWithSpotIdentifier:(int32_t)aSpotIdentifier;
+ (BOOL)hasFavoriteWithidentifier:(int32_t)anIdentifer;

+ (void)removeFavoriteWithIdentifer:(int32_t)anIdentifier;
+ (void)removeAllFavorites;

+ (NSArray *)fetchAllFavorites;
+ (NSArray *)fetchAllFavoriteSpots;


@end
