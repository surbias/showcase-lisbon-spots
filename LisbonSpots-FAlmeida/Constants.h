//
//  Constants.h
//  LisbonSpots-FAlmeida
//
//  Created by MAC02-SOFTINSA on 18/07/15.
//  Copyright (c) 2015 Formando FLAG. All rights reserved.
//

#ifndef LisbonSpots_FAlmeida_Constants_h
#define LisbonSpots_FAlmeida_Constants_h

//JSON Datasource URls
#define kMY_URL @"https://dl.dropboxusercontent.com/s/ssfbojoqmnpog1e/LisbonSpots.json"
#define kJOAO_URL @"https://dl.dropboxusercontent.com/u/3327071/LisbonSpots.json"

//Custom Colours
#define kCOLOR_TINT [UIColor colorWithRed:0.0/255.0 green:128.0/255.0 blue:255.0/255.0 alpha:1.0];
#define kCOLOR_SKY_BLUE [UIColor colorWithRed:102.0/255.0 green:204/255.0 blue:255/255.0 alpha:1]
#define kCOLOR_LAVENDER [UIColor colorWithRed:204.0/255.0 green:102.0/255.0 blue:255.0/255.0 alpha:1];
#define kCOLOR_CANTALOUPE [UIColor colorWithRed:255.0/255.0 green:204.0/255.0 blue:102.0/255.0 alpha:1]

//Navigation Themed Titles
#define kGALLERY_TITLE @"Lisbon Spots"
#define kMAP_TITLE @"Spots Map"
#define kFAVORITES_TITLE @"Favorite Spots"
#define kSETTINGS_TITLE @"Settings"

//Cell Identifiers
#define kCELL_NORMAL @"SpotCell"
#define kCELL_HEADER @"SpotHeader"

#define kCOLLECTION_CELL @"FavoriteCell"

//Coordinates
#define kCOORDINATES_ATRIUM_SALDANHA CLLocationCoordinate2DMake(38.733355, -9.144713)
#define kREGION_AXIS 5000

//Messages
#define kMESSAGE_FAVORITES @"Seems like you don't have favorites yet. You can add them by selecting the heart icon on the spot details."
#define kPROMPT_SETTINGS @"This will delete all current favorites. Are you sure?"
#define KBEFORE_PROMPT_SETTINGS @"You don't have any favorites to delete!"

#endif
