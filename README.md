# Lisbon Spots
Showcase iOS project

# Concept
A simple mobile app that displays a set of restaurants/bars in Lisbon both as Master/Detail List and visually through the usage of Apple Maps. 
Allows saving favorites to check on them later on as a Collection.

# Tecnical features
 - External service integration
 - Data storage using Core Data
 - Data visualization implementation using UITableView and UICollectionView
 - Apple Map usage (custom icons and dialogs)
 - Application preferences through NSUserDefaults
 
# Instalation
open the file LisbonSpots-FAlmeida.xcodeproj and build it using XCode
